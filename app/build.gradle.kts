plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.jetbrains.kotlin.android)
}

android {
    namespace = "com.vio.basemvvm"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.vio.basemvvm"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    flavorDimensions.add("type")

    productFlavors {
        create("appDev") {
            manifestPlaceholders["ad_app_id"] =  "ca-app-pub-3940256099942544~3347511713"
            buildConfigField("String", "Inter_splash", "\"ca-app-pub-3940256099942544/1033173712\"")
            buildConfigField(
                "String",
                "Banner_splash",
                "\"ca-app-pub-3940256099942544/6300978111\""
            )
            buildConfigField(
                "String",
                "AppOpen_resume",
                "\"ca-app-pub-3940256099942544/9257395921\""
            )
            buildConfigField(
                "String",
                "Native_language",
                "\"ca-app-pub-3940256099942544/2247696110\""
            )
            buildConfigField("String", "Native_language_2f", "\"ca-app-pub-3940256099942544/1044960115\"")
            buildConfigField("String", "Native_language_dup", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_language_dup_2f", "\"ca-app-pub-3940256099942544/1044960115\"")
            buildConfigField("String", "Native_onboarding", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_onboarding2f", "\"ca-app-pub-3940256099942544/1044960115\"")
            buildConfigField("String", "Native_onboarding2", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_onboarding3", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_onboarding_fullScreen", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Inter_Onboarding", "\"ca-app-pub-3940256099942544/1033173712\"")
            buildConfigField("String", "Native_OB12_fullscreen", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Banner", "\"ca-app-pub-3940256099942544/6300978111\"")
            buildConfigField("String", "native_permission", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Inter_Home", "\"ca-app-pub-3940256099942544/1033173712\"")
            buildConfigField("String", "Native_exit", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_compass", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Reward_Police_light", "\"ca-app-pub-3940256099942544/5224354917\"")
            buildConfigField("String", "Reward_Police_light", "\"ca-app-pub-3940256099942544/5224354917\"")
            buildConfigField("String", "Reward_edge_theme", "\"ca-app-pub-3940256099942544/5224354917\"")
            buildConfigField("String", "Reward_edge_apply", "\"ca-app-pub-3940256099942544/5224354917\"")

            setDimension("type")
        }

        create("appProduct") {
            manifestPlaceholders["ad_app_id"] =  "ca-app-pub-3940256099942544~3347511713"
            buildConfigField("String", "Inter_splash", "\"ca-app-pub-3940256099942544/1033173712\"")
            buildConfigField(
                "String",
                "Banner_splash",
                "\"ca-app-pub-3940256099942544/9214589741\""
            )
            buildConfigField(
                "String",
                "AppOpen_resume",
                "\"ca-app-pub-3940256099942544/9257395921\""
            )
            buildConfigField(
                "String",
                "Native_language",
                "\"ca-app-pub-3940256099942544/2247696110\""
            )
            buildConfigField("String", "Native_language_2f", "\"ca-app-pub-3940256099942544/1044960115\"")
            buildConfigField("String", "Native_language_dup", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_language_dup_2f", "\"ca-app-pub-3940256099942544/1044960115\"")
            buildConfigField("String", "Native_onboarding", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_onboarding2f", "\"ca-app-pub-3940256099942544/1044960115\"")
            buildConfigField("String", "Native_onboarding2", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_onboarding3", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_onboarding_fullScreen", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Inter_Onboarding", "\"ca-app-pub-3940256099942544/1033173712\"")
            buildConfigField("String", "Native_OB12_fullscreen", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Banner_Collap_Home", "\"ca-app-pub-3940256099942544/2014213617\"")
            buildConfigField("String", "Banner", "\"ca-app-pub-3940256099942544/9214589741\"")
            buildConfigField("String", "native_permission", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Inter_Home", "\"ca-app-pub-3940256099942544/1033173712\"")
            buildConfigField("String", "Inter_back", "\"ca-app-pub-3940256099942544/1033173712\"")
            buildConfigField("String", "Native_home", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_History_pressure", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_History_surgar", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Native_exit", "\"ca-app-pub-3940256099942544/2247696110\"")
            buildConfigField("String", "Banner_inline_news", "\"ca-app-pub-4584260126367940/4345254018\"")
            setDimension("type")
        }
    }

    buildFeatures {
        viewBinding = true
        buildConfig = true
    }
    signingConfigs {
        create("release") {
            keyAlias = "key_release"
            keyPassword = "Vinh0340"
            storeFile = file("key/key_release.jks")
            storePassword = "Vinh0340"
        }
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = signingConfigs.findByName("release")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.constraintlayout)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)

    implementation(libs.androidx.lifeCycle.process)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.rxAndroid)
    implementation(libs.androidx.navigation)
    implementation(libs.androidx.navigation.fragment)

    implementation("com.github.VioTechDev:VioAds:1.0.14")
    implementation("com.github.VioTechDev:VioLFO:1.0.3")
}