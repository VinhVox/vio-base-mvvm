package com.vio.basemvvm.helper

import android.content.Context
import com.vio.basemvvm.helper.base.Preferences

class ConfigPreferences(context: Context) : Preferences(context, "ConfigPreferences") {
    companion object{
        private const val LANGUAGE_CODE = "LANGUAGE_CODE"
    }
    //ads
    var isEnableUMP by booleanPref(AdsConfig.KEY_ENABLE_UMP, true)
    var languageCode by stringPref(LANGUAGE_CODE, "")

}
