package com.vio.basemvvm.ui.base

import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
abstract class BaseViewModel : ViewModel(), LifecycleEventObserver {

    private var disposable: CompositeDisposable = CompositeDisposable()

    val subscriptions: CompositeDisposable
        get() {
            if (disposable.isDisposed) disposable = CompositeDisposable()
            return disposable
        }

    fun clearSubscriptions() {
        disposable.dispose()
    }

    override fun onCleared() {
        disposable.dispose()
    }
}