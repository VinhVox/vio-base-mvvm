package com.vio.basemvvm.ui.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewbinding.ViewBinding
import com.vio.basemvvm.helper.ConfigPreferences
import com.vio.basemvvm.ui.extension.applyStatusBarLightTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import java.util.Locale

abstract class BaseFragment<B : ViewBinding> : Fragment(), CoroutineScope by CoroutineScope(
    Dispatchers.Main
) {
    protected var currentContext: Context? = null
    protected lateinit var viewBinding: B
        private set

    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> B
    protected open val isLightTheme: Boolean = false
    private var configPreferences: ConfigPreferences? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        currentContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        currentContext?.let {
            configPreferences = ConfigPreferences(it)
            configPreferences?.languageCode?.let { languageCode ->
                if (languageCode.isNotEmpty())
                    changeLanguage(languageCode)
            }
        }
    }

    protected fun changeLanguage(languageCode: String) {
        val config = Configuration()
        val locale = Locale(languageCode)
        Locale.setDefault(locale)
        config.locale = locale
        currentContext?.resources?.updateConfiguration(config, null)
    }

    @CallSuper
    open fun onViewBindingCreated(savedInstanceState: Bundle?) {
        activity?.applyStatusBarLightTheme(isLightTheme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = bindingInflater.invoke(inflater, container, false)
        onViewBindingCreated(savedInstanceState)
        return viewBinding.root
    }

    protected fun actionBackPressFinishApp() {
        (currentContext as FragmentActivity).let { fragmentManager ->
            fragmentManager.onBackPressedDispatcher
                .addCallback(this, object : OnBackPressedCallback(true) {
                    override fun handleOnBackPressed() {
                        fragmentManager.finishAndRemoveTask()
                    }
                }
                )
        }
    }

    override fun onDestroyView() {
        coroutineContext[Job]?.cancel()
        super.onDestroyView()
    }

    override fun onDetach() {
        currentContext = null
        super.onDetach()
    }

    protected fun getIntent(): Intent? {
        return currentContext?.let {
            (currentContext as Activity).intent
        } ?: run {
            null
        }
    }
}