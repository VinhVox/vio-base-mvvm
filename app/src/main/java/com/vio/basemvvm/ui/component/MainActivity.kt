package com.vio.basemvvm.ui.component

import android.view.LayoutInflater
import com.vio.basemvvm.databinding.ActivityMainBinding
import com.vio.basemvvm.ui.base.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding>() {
    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        get() = ActivityMainBinding::inflate
}