package com.vio.basemvvm.ui.component.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.vio.basemvvm.ui.component.main.home.HomeFragment
import com.vio.basemvvm.ui.component.main.settings.SettingsFragment
import com.vio.basemvvm.ui.component.main.tool.ToolsFragment


class HomeViewPagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> HomeFragment.newInstance()
            1 -> ToolsFragment.newInstance()
            2 -> SettingsFragment.newInstance()
            else -> throw IllegalStateException("Invalid position")
        }
    }

    override fun getItemCount(): Int {
        return 3
    }
}
