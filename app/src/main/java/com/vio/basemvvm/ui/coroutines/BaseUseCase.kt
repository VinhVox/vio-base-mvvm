package com.vio.basemvvm.ui.coroutines

import kotlinx.coroutines.CoroutineDispatcher


abstract class BaseUseCase(
    executionDispatcher: CoroutineDispatcher
) {
    protected val dispatcher = executionDispatcher

    abstract fun logException(e: Exception)
}