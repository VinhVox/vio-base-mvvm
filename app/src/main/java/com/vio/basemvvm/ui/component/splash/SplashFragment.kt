package com.vio.basemvvm.ui.component.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vio.basemvvm.R
import com.vio.basemvvm.databinding.FragmentSplashBinding
import com.vio.basemvvm.ui.base.BaseFragment

class SplashFragment : BaseFragment<FragmentSplashBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentSplashBinding
        get() = FragmentSplashBinding::inflate
    override fun onViewBindingCreated(savedInstanceState: Bundle?) {
        super.onViewBindingCreated(savedInstanceState)
        viewBinding.button3.setOnClickListener {
            findNavController().navigate(R.id.action_splashFragment_to_onboardingFragment)
        }
    }
}