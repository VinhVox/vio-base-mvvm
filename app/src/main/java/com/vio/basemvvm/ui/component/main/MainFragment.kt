package com.vio.basemvvm.ui.component.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.vio.basemvvm.databinding.FragmentMainBinding
import com.vio.basemvvm.ui.base.BaseFragment
import com.vio.basemvvm.ui.component.main.adapter.HomeViewPagerAdapter
import com.vio.basemvvm.ui.component.main.home.HomeFragment
import com.vio.basemvvm.ui.component.main.settings.SettingsFragment
import com.vio.basemvvm.ui.component.main.tool.ToolsFragment

class MainFragment : BaseFragment<FragmentMainBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMainBinding
        get() = FragmentMainBinding::inflate

    private lateinit var adapter: HomeViewPagerAdapter
    private var listOfFragments = ArrayList<Fragment>()
    private var tabPosition = 0

    override fun onViewBindingCreated(savedInstanceState: Bundle?) {
        super.onViewBindingCreated(savedInstanceState)
        setUpBottomBar()
    }

    private fun setUpBottomBar() {
        activity?.let {
            listOfFragments.add(HomeFragment.newInstance())
            listOfFragments.add(ToolsFragment.newInstance())
            listOfFragments.add(SettingsFragment.newInstance())
            adapter = HomeViewPagerAdapter(it)
            viewBinding.homeViewPager.offscreenPageLimit = 3
            viewBinding.homeViewPager.isSaveEnabled = false
            viewBinding.homeViewPager.isUserInputEnabled = false
            viewBinding.homeViewPager.adapter = adapter
            viewBinding.homeViewPager.setCurrentItem(
                tabPosition, false
            )
            when (tabPosition) {
                0 -> viewBinding.rdHome1.isChecked = true
                1 -> viewBinding.rdHome2.isChecked = true
                2 -> viewBinding.rdHome3.isChecked = true
            }
        }

        viewBinding.rdBottomBar.setOnCheckedChangeListener { _, id ->
            when (id) {
                viewBinding.rdHome1.id -> {
                    tabPosition = 0
                    viewBinding.homeViewPager.setCurrentItem(tabPosition, true)
                }

                viewBinding.rdHome2.id -> {
                    tabPosition = 1
                    viewBinding.homeViewPager.setCurrentItem(tabPosition, true)
                }

                viewBinding.rdHome3.id -> {
                    tabPosition = 2
                    viewBinding.homeViewPager.setCurrentItem(tabPosition, true)
                }

            }
        }

    }
}