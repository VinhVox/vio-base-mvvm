package com.vio.basemvvm.ui.component.main.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vio.basemvvm.R
import com.vio.basemvvm.databinding.FragmentSettingsBinding
import com.vio.basemvvm.ui.base.BaseFragment

class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentSettingsBinding
        get() = FragmentSettingsBinding::inflate

    companion object {
        const val FRAGMENT_TAG = "OnBoardingSlide1ReloadFragment"

        fun newInstance(): SettingsFragment {
            return newInstance(FRAGMENT_TAG)
        }

        private fun newInstance(tag: String): SettingsFragment {
            val fragment = SettingsFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewBindingCreated(savedInstanceState: Bundle?) {
        super.onViewBindingCreated(savedInstanceState)
        viewBinding.button5.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_previewFragment)
        }
    }
}