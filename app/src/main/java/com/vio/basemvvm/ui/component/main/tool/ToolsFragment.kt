package com.vio.basemvvm.ui.component.main.tool

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vio.basemvvm.R
import com.vio.basemvvm.databinding.FragmentToolsBinding
import com.vio.basemvvm.ui.base.BaseFragment

class ToolsFragment : BaseFragment<FragmentToolsBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentToolsBinding
        get() = FragmentToolsBinding::inflate

    companion object {
        const val FRAGMENT_TAG = "OnBoardingSlide1ReloadFragment"

        fun newInstance(): ToolsFragment {
            return newInstance(FRAGMENT_TAG)
        }

        private fun newInstance(tag: String): ToolsFragment {
            val fragment = ToolsFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onViewBindingCreated(savedInstanceState: Bundle?) {
        super.onViewBindingCreated(savedInstanceState)
        viewBinding.button5.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_previewFragment)
        }
    }
}