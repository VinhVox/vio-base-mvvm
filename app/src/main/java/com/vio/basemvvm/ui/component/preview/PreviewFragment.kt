package com.vio.basemvvm.ui.component.preview

import android.view.LayoutInflater
import android.view.ViewGroup
import com.vio.basemvvm.databinding.FragmentPreviewBinding
import com.vio.basemvvm.ui.base.BaseFragment

class PreviewFragment : BaseFragment<FragmentPreviewBinding>() {
    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentPreviewBinding
        get() = FragmentPreviewBinding::inflate
}