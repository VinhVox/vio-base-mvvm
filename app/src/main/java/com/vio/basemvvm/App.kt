package com.vio.basemvvm

import android.app.Application
import androidx.lifecycle.ProcessLifecycleOwner
import com.ads.admob.admob.AdmobFactory
import com.ads.admob.config.NetworkProvider
import com.ads.admob.config.VioAdConfig
import com.ads.admob.config.VioAdjustConfig
import com.ads.admob.helper.appoppen.AppResumeAdConfig
import com.ads.admob.helper.appoppen.AppResumeAdHelper
import com.vio.basemvvm.ui.component.splash.SplashFragment

class App : Application() {
    companion object {
        var appResumeAdHelper: AppResumeAdHelper? = null
            private set
    }

    override fun onCreate() {
        super.onCreate()
        configAds()
        appResumeAdHelper = initAppOpenAd()
    }

    private fun configAds() {
        val vioAdjustConfig =
            VioAdjustConfig.Build("adjust_token", true).build()
        val vioAdConfig = VioAdConfig.Builder(vioAdjustConfig = vioAdjustConfig)
            .buildVariantProduce(false)
            .mediationProvider(NetworkProvider.ADMOB)
            .listTestDevices(ArrayList())
            .build()
        AdmobFactory.getInstance().initAdmob(this, vioAdConfig)
    }

    private fun initAppOpenAd(): AppResumeAdHelper {
        val listClassInValid = mutableListOf<Class<*>>()
        listClassInValid.add(SplashFragment::class.java)
        val config = AppResumeAdConfig(
            idAds = BuildConfig.AppOpen_resume,
            listClassInValid = listClassInValid
        )
        return AppResumeAdHelper(
            application = this,
            lifecycleOwner = ProcessLifecycleOwner.get(),
            config = config
        )
    }

}